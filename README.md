# Scheduled Backup #

This is the repository for the scheduled backup script. The scheduled backup script is used to retrieve the entire Salesforce org and track using Git.

### How do I get set up? ###

* Create a repository in Bitbucket (http://bitbucket.org/)
* Log in to CS Bamboo instance (https://ci.securesherpa.org/bamboo/)
* Go to Create > Clone an existing plan
* For Plan to clone, select Backup (under Genomics)
* Select the Destination project to do the Backup
* Fill up other necessary details
* Click Create
* Go to Actions > Configure Plan > Variables
* Fill in the following:
 * * FORCEVERSION_SCM     : the link to the Bitbucket repo
 * * SALESFORCE_USERNAME  : Salesforce admin username
 * * SALESFORCE_PASSWORD  : Salesforce admin password
 * * SALESFORCE_URL       : test.salesforce.com or salesforce.com

### Who do I talk to? ###

* Rescian Rey
* Christian Sta. Ana