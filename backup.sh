#!/usr/bin/env bash
#
# Author:       Rescian Rey
# Description:  A script for the Bamboo server to backup a whole Salesforce org.
#               Retrieves all the components of an org and track it via Git.
# History:
#   31.AUG.2015     Rescian Rey     Created this script

# Description:  Reads a file and puts in an array. Takes two arguments.
# Parameters:   $1 - file to be read
#               $2 - array to put the read lines
function readFile() {
    unset IFS
    IFS=$'\n' read -d '' -r -a $2 < $1
}

# Description:  Splits a string by specified character.
# Parameters:   $1 - input string
#               $2 - separator character
#               $3 - array to put the separated strings
function split() {
    unset IFS
    IFS=$2 read -ra $3 <<< $1
}

# Description:  String substitution
# Parameters:   $1 - input string
#               $2 - regex to replace
#               $3 - String to replace with
function replace(){
    echo $1|sed -re 's/'$2'/'$3'/g'
}


DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ANT_BUILD=${DIR}$'/build.xml'
ANT_LIB=${DIR}$'/ant_lib'
RETRIEVED_DIR="src"
PCKG="package.xml"
TMP="tmp.txt"
PROFILE_BUNDLE=("Profile" "PermissionSet" "CustomObject" "ApexClass" "ApexPage" "ApexComponent" "HomePageComponent" "CustomApplication" "CustomApplicationComponent" "CustomTab" "RecordType")
PACKAGE_XML_HEADER='<?xml version="1.0" encoding="UTF-8"?><Package xmlns="http://soap.sforce.com/2006/04/metadata">'
PACKAGE_XML_FOOTER='</Package>'

metadata=()
is_folder_based=()
is_metadata_name=false
is_metadata_type=false
available_profile_comps=()

echo "Pulling from repo..."
ant gitPull -buildfile ${ANT_BUILD} -lib ${ANT_LIB}

echo "Emptying current src folder..."
ant emptyFolder -Dfolder=${RETRIEVED_DIR} -buildfile ${ANT_BUILD} -lib ${ANT_LIB}

echo "Retrieving available metadata..."
ant describeMetadata -buildfile ${ANT_BUILD} -lib ${ANT_LIB} > ${TMP}

# Parse the result of describeMetadata
readFile ${TMP} descMetadata_resp

# Get available metadata, determine if folder-based or not
for i in "${!descMetadata_resp[@]}"; do
    line=${descMetadata_resp[$i]}
    if [[ $line == *"XMLName:"* ]]; then
        split "$line" ' ' words
        metadata=(${metadata[@]} ${words[2]})
    elif [[ $line == *"InFolder:"* ]]; then
        split "$line" ' ' words
        is_folder_based=(${is_folder_based[@]} ${words[2]})
    fi
done

echo "Metadata: ${metadata[@]}"
echo ""
echo "Querying components for each metadata..."
echo ""

mkdir ${RETRIEVED_DIR}

# Retrieval of metadata
for i in "${!metadata[@]}"; do
    # special handling for email template
    if [ ${is_folder_based[i]} = "true" ] && [ "${metadata[i]}" != "EmailTemplate" ]; then
        # write the package.xml
        components_xml=${PACKAGE_XML_HEADER}
        components_xml=${components_xml}'<type>'

        echo "Retrieving components: ${metadata[$i]}"

        # lists components inside folders
        ant listMetadata -Dsf.metadataType=${metadata[i]}"Folder" -buildfile ${ANT_BUILD} -lib ${ANT_LIB} > ${TMP}
        readFile ${TMP} listMetadata_resp

        # construct package.xml
        for j in "${!listMetadata_resp[@]}"; do
            line=${listMetadata_resp[$j]}
            if [[ $line == *"FullName/Id:"* ]]; then
                folder_name=$(replace "$line" '\[sf:listMetadata\]\sFullName\/Id:\s' '')
                split "$folder_name" '/ ' words
                components_xml=${components_xml}'<members>'${words[0]}'</members>'

                ant listMetadata -Dsf.metadataType=${metadata[i]} -Dsf.folder=${words[0]} -buildfile ${ANT_BUILD} -lib ${ANT_LIB} > ${TMP}
                readFile ${TMP} listMetadata_resp_2

                for k in "${!listMetadata_resp_2[@]}"; do
                    line2=${listMetadata_resp_2[$k]}
                    if [[ $line2 == *"FullName/Id:"* ]]; then
                        comp_name=$(replace "$line2" '\[sf:listMetadata\]\sFullName\/Id:\s' '')
                        comp_name=$(replace "$comp_name" '(.*\/.*)\/.*' '\1')
                        components_xml=${components_xml}'<members>'${comp_name}'</members>'
                    fi
                done
            fi
        done

        components_xml=${components_xml}'<name>'${metadata[i]}'</name>'
        components_xml=${components_xml}'</type>'
        components_xml=${components_xml}${PACKAGE_XML_FOOTER}
        echo ${components_xml} > ${PCKG}

        # retrieve package
        ant retrieveUnpackaged -Dsf.unpackaged=${PCKG} -Dsf.maxPoll=200 -Dsf.retrieveTarget=${RETRIEVED_DIR} -buildfile ${ANT_BUILD} -lib ${ANT_LIB}
    else
        for_later=false
        for item in "${!PROFILE_BUNDLE[@]}"; do
            if [ "${PROFILE_BUNDLE[$item]}" = "${metadata[$i]}" ]; then
                for_later=true
            break
          fi
        done

        if [ ${for_later} = true ]; then
            available_profile_comps=(${available_profile_comps[@]} ${metadata[$i]})
        else
            echo "Retrieving components: ${metadata[$i]}"
            # call bulk retrieve for non folder, non profile-based components
            ant bulkRetrieve -Dsf.metadataType=${metadata[$i]} -Dsf.retrieveTarget=${RETRIEVED_DIR} -buildfile ${ANT_BUILD} -lib ${ANT_LIB}
        fi
    fi
done

echo "Retrieving profile-dependent components... "
echo "Metadata: ${available_profile_comps[@]}"

# Retrieve profile-dependent components all at once
components_xml=${PACKAGE_XML_HEADER}
for comp in "${!available_profile_comps[@]}"; do
    echo 'Retrieving components: '${available_profile_comps[$comp]}
    components_xml=${components_xml}'<type>'
    ant listMetadata -Dsf.metadataType=${available_profile_comps[$comp]} -buildfile ${ANT_BUILD} -lib ${ANT_LIB} > ${TMP}
    readFile ${TMP} listMetadata_resp

    for k in "${!listMetadata_resp[@]}"; do
        line=${listMetadata_resp[$k]}
        if [[ $line == *"FullName/Id:"* ]]; then
            comp_name=$(replace "$line" '\[sf:listMetadata\]\sFullName\/Id:\s' '')
            comp_name=$(replace "$comp_name" '\/.*' '')
            components_xml=$components_xml'<members>'${comp_name}'</members>'
        fi
    done

    components_xml=${components_xml}'<name>'${available_profile_comps[$comp]}'</name>'
    components_xml=${components_xml}'</type>'
done

# create package.xml
components_xml=${components_xml}${PACKAGE_XML_FOOTER}
echo ${components_xml} > ${PCKG}

ant retrieveUnpackaged -Dsf.unpackaged=${PCKG} -Dsf.maxPoll=200 -Dsf.retrieveTarget=${RETRIEVED_DIR} -buildfile ${ANT_BUILD} -lib ${ANT_LIB}
rm ${PCKG}
rm ${TMP}

echo "Pushing to repo..."
ant gitCommitPush -DcommitMessage="backup $(date +%Y-%m-%d:%H:%M:%S)" -buildfile ${ANT_BUILD} -lib ${ANT_LIB}    